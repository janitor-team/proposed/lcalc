#ifndef Lriemannsiegel_blfi_H
#define Lriemannsiegel_blfi_H

#include "L.h"

using namespace std;


// Computes the kernel function in BLFI
// XXXXXX Needs to be made multiprecision
inline Double blfi_kernel(Double x) {

        // return 1.;

        Double one=Double(1);
        if( x < 0 ) return Double(0);
        else if(x < 1.e-5) {
                return ((((one/39916800*x + one/362880)*x + one/5040)*x + one/120) *x + one/6) * x + 1;
        }
        else {
                Double sqrt_x = sqrt(x);
                return sinh(sqrt_x)/sqrt_x;
        }
}

#endif
